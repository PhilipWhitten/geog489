# README #

This README is designed for students of Penn State World Campus GEOG 489 GIS Programming course.

### What is this repository for? ###

* All course practice exercise solution source code and solution explanations are available for viewing and editing in this git repository.

* C# Practice Solution Code samples are found [here][csharpcode].
* C# Practice Exercise Explanations are found [here][csharp].

* VB.NET Practice Solution Code samples are found [here][vbnetcode].
* VB.NET Practice Exercise Explanations are found [here][vbnet].

* You are encouraged to browse the repository, fork the repository and contribute changes if you uncover a flaw in the source code or explanation text.
* Version 0.1


### How do I get set up? ###

* Summary of set up

### Contribution guidelines ###

* Other guidelines

### Who do I talk to? ###

* The repository owner and instructor of GEOG489 is Andrew Murdoch (ahm126@psu.edu)


The README is written using Markdown text formatting:

[Learn Markdown](https://bitbucket.org/tutorials/markdowndemo)

[csharpcode]: https://bitbucket.org/ahm126/geog489/src/master/PracticeSolutions/C%23/?at=master
[vbnetcode]: https://bitbucket.org/ahm126/geog489/src/master/PracticeSolutions/VB.NET/?at=master
[csharp]: https://bitbucket.org/ahm126/geog489/wiki/browse/C%23
[vbnet]: https://bitbucket.org/ahm126/geog489/wiki/browse/VB.NET

[csharpcode_old]: /ahm126/geog489/src/master/PracticeSolutions/C%23/?at=master
[vbnetcode_old]: /ahm126/geog489/src/master/PracticeSolutions/VB.NET/?at=master
[csharp_old]: /ahm126/geog489/wiki/browse/C%23
[vbnet_old]: /ahm126/geog489/wiki/browse/VB.NET